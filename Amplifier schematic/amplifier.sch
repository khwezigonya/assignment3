EESchema Schematic File Version 4
LIBS:proj-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	10750 3450 10300 3450
Text GLabel 10300 3450 0    50   Output ~ 0
M1Y2
Wire Wire Line
	10750 3350 10600 3350
Text GLabel 10600 3350 0    50   Output ~ 0
M1Y1
$Comp
L power:GND #PWR06
U 1 1 60DDA2F8
P 10350 3150
F 0 "#PWR06" H 10350 2900 50  0001 C CNN
F 1 "GND" H 10355 2977 50  0000 C CNN
F 2 "" H 10350 3150 50  0001 C CNN
F 3 "" H 10350 3150 50  0001 C CNN
	1    10350 3150
	0    1    1    0   
$EndComp
Text GLabel 10350 3250 0    50   Input ~ 0
5V
Wire Wire Line
	10350 3250 10750 3250
Wire Wire Line
	10750 3150 10350 3150
Wire Wire Line
	10750 3050 10050 3050
Wire Wire Line
	10750 2950 10500 2950
Text GLabel 10500 2950 0    50   Input ~ 0
M1A
$Comp
L Connector:Conn_01x06_Male J4
U 1 1 60C5F23D
P 10950 3150
F 0 "J4" H 10922 3124 50  0000 R CNN
F 1 "M1" H 10922 3033 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x06_P1.27mm_Vertical" H 10950 3150 50  0001 C CNN
F 3 "~" H 10950 3150 50  0001 C CNN
	1    10950 3150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7250 4700 7250 4550
Text GLabel 7250 4550 1    50   Input ~ 0
3.3V
Wire Wire Line
	8800 4100 8550 4100
Text GLabel 8550 4100 0    50   Output ~ 0
M2Y2
Text GLabel 8800 4000 0    50   Output ~ 0
M2Y1
Wire Wire Line
	8450 3900 8800 3900
Text GLabel 8450 3900 0    50   Input ~ 0
5V
Wire Wire Line
	8800 3800 8300 3800
$Comp
L power:GND #PWR05
U 1 1 60DE0DA7
P 8300 3800
F 0 "#PWR05" H 8300 3550 50  0001 C CNN
F 1 "GND" H 8305 3627 50  0000 C CNN
F 2 "" H 8300 3800 50  0001 C CNN
F 3 "" H 8300 3800 50  0001 C CNN
	1    8300 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	8800 3600 8000 3600
Text GLabel 8000 3600 0    50   Input ~ 0
M2A
Wire Wire Line
	8600 3700 8800 3700
Text GLabel 8600 3700 0    50   Input ~ 0
M2B
$Comp
L Connector:Conn_01x06_Male J3
U 1 1 60CB5933
P 9000 3800
F 0 "J3" H 8972 3774 50  0000 R CNN
F 1 "M2" H 8972 3683 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x06_P1.27mm_Vertical" H 9000 3800 50  0001 C CNN
F 3 "~" H 9000 3800 50  0001 C CNN
	1    9000 3800
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR016
U 1 1 60EC4A39
P 5500 7400
F 0 "#PWR016" H 5500 7150 50  0001 C CNN
F 1 "GND" H 5505 7227 50  0000 C CNN
F 2 "" H 5500 7400 50  0001 C CNN
F 3 "" H 5500 7400 50  0001 C CNN
	1    5500 7400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5500 7200 5500 7400
Text GLabel 5950 7000 2    50   Input ~ 0
3.3V
Wire Wire Line
	5700 7000 5950 7000
NoConn ~ 5400 7200
NoConn ~ 7150 5300
NoConn ~ 7050 5300
NoConn ~ 5300 7200
Wire Wire Line
	6850 5000 6400 5000
Text GLabel 6400 5000 0    50   Output ~ 0
M1Out2
Wire Wire Line
	10100 5750 9850 5750
Wire Wire Line
	10100 5650 9550 5650
Wire Wire Line
	10100 5550 9950 5550
Wire Wire Line
	10100 5450 9550 5450
Text GLabel 9550 5450 0    50   Input ~ 0
M1Out1
Text GLabel 9950 5550 0    50   Input ~ 0
M1Out2
Text GLabel 9550 5650 0    50   Input ~ 0
M2Out1
Text GLabel 9850 5750 0    50   Input ~ 0
M2Out2
$Comp
L Connector:Conn_01x04_Male J5
U 1 1 60F19AED
P 10300 5650
F 0 "J5" H 10272 5532 50  0000 R CNN
F 1 "Output" H 10272 5623 50  0000 R CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x04_P1.27mm_Vertical" H 10300 5650 50  0001 C CNN
F 3 "~" H 10300 5650 50  0001 C CNN
	1    10300 5650
	-1   0    0    1   
$EndComp
Text GLabel 5450 5650 2    50   Input ~ 0
3.3V
Text GLabel 5600 5450 2    50   Input ~ 0
M1Y1
Text GLabel 6100 6800 2    50   Input ~ 0
M2Y2
Wire Wire Line
	5500 6600 5500 6450
Text GLabel 5500 6450 1    50   Input ~ 0
3.3V
Wire Wire Line
	5700 6800 6100 6800
Text GLabel 7850 4900 2    50   Input ~ 0
M1Y2
Wire Wire Line
	7450 5100 7700 5100
Wire Wire Line
	7450 4900 7850 4900
Text GLabel 7700 5100 2    50   Input ~ 0
3.3V
Wire Wire Line
	7250 5300 7250 5500
$Comp
L power:GND #PWR017
U 1 1 60EB5C09
P 7250 5500
F 0 "#PWR017" H 7250 5250 50  0001 C CNN
F 1 "GND" H 7255 5327 50  0000 C CNN
F 2 "" H 7250 5500 50  0001 C CNN
F 3 "" H 7250 5500 50  0001 C CNN
	1    7250 5500
	-1   0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM741 U7
U 1 1 60EB5C03
P 7150 5000
F 0 "U7" H 7494 5046 50  0000 L CNN
F 1 "LM741" H 7494 4955 50  0000 L CNN
F 2 "Package_SO:SOP-8_3.9x4.9mm_P1.27mm" H 7200 5050 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 7300 5150 50  0001 C CNN
	1    7150 5000
	-1   0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM741 U6
U 1 1 60EC4A33
P 5400 6900
F 0 "U6" H 5744 6946 50  0000 L CNN
F 1 "LM741" H 5744 6855 50  0000 L CNN
F 2 "Package_SO:SOP-8_3.9x4.9mm_P1.27mm" H 5450 6950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 5550 7050 50  0001 C CNN
	1    5400 6900
	-1   0    0    -1  
$EndComp
NoConn ~ 2300 7350
NoConn ~ 2400 7350
NoConn ~ 4900 5850
NoConn ~ 4800 5850
Wire Wire Line
	1700 5700 1700 5750
Wire Wire Line
	2350 5700 2350 5750
Wire Wire Line
	2350 5700 1700 5700
Wire Wire Line
	2750 5700 2750 6200
Wire Wire Line
	2750 5200 2350 5200
Wire Wire Line
	2750 5400 2750 5200
Wire Wire Line
	1200 5500 1200 6200
Wire Wire Line
	2350 5200 2350 5300
Connection ~ 2350 5200
Wire Wire Line
	2000 5200 2350 5200
Connection ~ 2350 5700
Wire Wire Line
	2350 5600 2350 5700
Wire Wire Line
	1700 6200 1700 6300
Wire Wire Line
	1200 6200 1700 6200
Connection ~ 1700 6200
Wire Wire Line
	2750 6200 2350 6200
Wire Wire Line
	2350 6200 1700 6200
Connection ~ 2350 6200
Wire Wire Line
	2350 6050 2350 6200
Connection ~ 1700 5700
Wire Wire Line
	1700 5500 1700 5700
Wire Wire Line
	1700 6050 1700 6200
$Comp
L power:GND #PWR03
U 1 1 60C0E864
P 1700 6300
F 0 "#PWR03" H 1700 6050 50  0001 C CNN
F 1 "GND" H 1705 6127 50  0000 C CNN
F 2 "" H 1700 6300 50  0001 C CNN
F 3 "" H 1700 6300 50  0001 C CNN
	1    1700 6300
	1    0    0    -1  
$EndComp
Text GLabel 900  5200 0    50   Input ~ 0
5V
Wire Wire Line
	1200 5200 1400 5200
Wire Wire Line
	900  5200 1200 5200
Connection ~ 1200 5200
$Comp
L Device:CP1 C6
U 1 1 60BF276D
P 1200 5350
F 0 "C6" H 1315 5396 50  0000 L CNN
F 1 "10u" H 1315 5305 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x3" H 1200 5350 50  0001 C CNN
F 3 "~" H 1200 5350 50  0001 C CNN
	1    1200 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C7
U 1 1 60BECFEF
P 1700 5900
F 0 "C7" H 1815 5946 50  0000 L CNN
F 1 "10u" H 1815 5855 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x3" H 1700 5900 50  0001 C CNN
F 3 "~" H 1700 5900 50  0001 C CNN
	1    1700 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 60BE784A
P 2350 5900
F 0 "R8" H 2420 5946 50  0000 L CNN
F 1 "190" H 2420 5855 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2280 5900 50  0001 C CNN
F 3 "~" H 2350 5900 50  0001 C CNN
	1    2350 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 60BE7002
P 2350 5450
F 0 "R7" H 2420 5496 50  0000 L CNN
F 1 "120" H 2420 5405 50  0000 L CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 2280 5450 50  0001 C CNN
F 3 "~" H 2350 5450 50  0001 C CNN
	1    2350 5450
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C8
U 1 1 60BD6C57
P 2750 5550
F 0 "C8" H 2865 5596 50  0000 L CNN
F 1 "100u" H 2865 5505 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x3" H 2750 5550 50  0001 C CNN
F 3 "~" H 2750 5550 50  0001 C CNN
	1    2750 5550
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LT1117-3.3 U4
U 1 1 60BCE08C
P 1700 5200
F 0 "U4" H 1700 5442 50  0000 C CNN
F 1 "LT1117-3.3" H 1700 5351 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 1700 5200 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/1117fd.pdf" H 1700 5200 50  0001 C CNN
	1    1700 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 7050 1650 7050
Wire Wire Line
	5100 6900 4800 6900
Wire Wire Line
	4600 5550 4150 5550
Text GLabel 1650 7050 0    50   Output ~ 0
M2Out1
Text GLabel 4800 6900 0    50   Output ~ 0
M2Out2
Text GLabel 4150 5550 0    50   Output ~ 0
M1Out1
$Comp
L Amplifier_Operational:LM741 U3
U 1 1 60E5149F
P 4900 5550
F 0 "U3" H 5244 5596 50  0000 L CNN
F 1 "LM741" H 5244 5505 50  0000 L CNN
F 2 "Package_SO:SOP-8_3.9x4.9mm_P1.27mm" H 4950 5600 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 5050 5700 50  0001 C CNN
	1    4900 5550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5000 5850 5000 6050
Wire Wire Line
	5200 5450 5600 5450
Wire Wire Line
	5200 5650 5450 5650
Text GLabel 5000 5100 1    50   Input ~ 0
3.3V
Wire Wire Line
	5000 5250 5000 5100
Text GLabel 3100 6950 2    50   Input ~ 0
M2Y1
Wire Wire Line
	2500 6750 2500 6600
Text GLabel 2500 6600 1    50   Input ~ 0
3.3V
Wire Wire Line
	2700 7150 2950 7150
Wire Wire Line
	2700 6950 3100 6950
Text GLabel 2950 7150 2    50   Input ~ 0
3.3V
Wire Wire Line
	2500 7350 2500 7550
$Comp
L power:GND #PWR07
U 1 1 60EBF785
P 2500 7550
F 0 "#PWR07" H 2500 7300 50  0001 C CNN
F 1 "GND" H 2505 7377 50  0000 C CNN
F 2 "" H 2500 7550 50  0001 C CNN
F 3 "" H 2500 7550 50  0001 C CNN
	1    2500 7550
	-1   0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LM741 U5
U 1 1 60EBF77F
P 2400 7050
F 0 "U5" H 2744 7096 50  0000 L CNN
F 1 "LM741" H 2744 7005 50  0000 L CNN
F 2 "Package_SO:SOP-8_3.9x4.9mm_P1.27mm" H 2450 7100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm741.pdf" H 2550 7200 50  0001 C CNN
	1    2400 7050
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 60E52321
P 5000 6050
F 0 "#PWR04" H 5000 5800 50  0001 C CNN
F 1 "GND" H 5005 5877 50  0000 C CNN
F 2 "" H 5000 6050 50  0001 C CNN
F 3 "" H 5000 6050 50  0001 C CNN
	1    5000 6050
	-1   0    0    -1  
$EndComp
Text GLabel 3050 5200 2    50   Output ~ 0
3.3V
Wire Wire Line
	3050 5200 2750 5200
Connection ~ 2750 5200
$EndSCHEMATC
